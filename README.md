# Bigquery Terrafom in Docker

This project is to provision Bigquery resource with Terraform+gcloud in docker environment.  

Examples is in bq-plan folder.

## Getting Started

1. Build with  
`docker build -t imagename:tag .`  
or pull from repo. 

2. Move Bigquery service account into bq-plan folder. 

3. Edit content in bq-plan folder.

4. Terraform plan using  
`docker run --rm -it -v C:\path\to\bq-plan:/gcp-terraform imagename:tag plan`  

5. Terraform apply using  
`docker run --rm -it -v C:\path\to\bq-plan:/gcp-terraform imagename:tag apply`

6. Terraform destroy using 
`docker run --rm -it -v C:\path\to\bq-plan:/gcp-terraform imagename:tag destroy`