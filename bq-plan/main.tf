# Create local variable
locals {
    projectid = "project-id"
}

# Specify provider
provider "google"{
  project     = local.projectid
  credentials = file("${path.module}/service-account.json")
}

# Create dataset
resource "google_bigquery_dataset" "example_dataset" {
  dataset_id                  = "example_dataset"
  description                 = "This is a test description"
  location                    = "US"
  project                     = local.projectid
}

# create table
resource "google_bigquery_table" "example_table" {
  dataset_id = google_bigquery_dataset.example_dataset.dataset_id
  table_id   = "sometable"

  time_partitioning {
    type = "DAY"
  }

  schema = file("${path.module}/example_table.json")

}